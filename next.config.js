/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: false,
  distDir: 'build',
}

module.exports = nextConfig
