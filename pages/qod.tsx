import React, { useEffect, useState } from 'react'
import type { NextPage } from 'next'
import axios from 'axios'
import LoaderIcon from '../components/loaderIcon'
import Head from 'next/head'
const QOD: NextPage = () => {
    const [quote, setQuote] = useState<any>(null)
    useEffect(() => {
        axios.get('/api/randomQuote').then(res => {
            setQuote(res.data[0]);
        })
    }, [])

    let shareDataDummy = {
        title: 'MDN',
        text: 'Learn web development on MDN!',
        url: 'https://developer.mozilla.org',
    }
    const shareQuote = () => {
        navigator.share({
            title: 'Quote',
            text: quote.q,
            url: 'https://mukeshmehra.netlify.app/qod'
        })
    }
    if (quote !== null) {
        return <><Head>
            <title>New Quote Everyday!</title>
            <meta name="description" content="A new quote is fetched everyday!" />
        </Head>
            <div className="container quote">
                <h1>Quote of the day!</h1>
                <h2 >{quote?.q}</h2>
                <h4><i>By {quote?.a}</i></h4>
                <div className="share">
                    {navigator.canShare && navigator.canShare(shareDataDummy) && (
                        <i className="fa fa-share-alt fa-2x" aria-hidden="true" onClick={shareQuote}></i>
                    )}
                </div>
            </div>
        </>
    } else {
        return <LoaderIcon></LoaderIcon>
    }

}
export default QOD