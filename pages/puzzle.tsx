import React, { useEffect, useState } from 'react'
import Line from '../components/puzzle/Line';
import styles from '../styles/Puzzle.module.css';
import { AnsType, data } from '../components/puzzle/dataTypes';
import axios from 'axios';
import { ToastContainer, toast } from 'react-toast'
import LoaderIcon from '../components/loaderIcon';
import ModalScreen from '../components/ModalScreen';
import Tile from '../components/puzzle/Tile';
import Head from 'next/head';

const Puzzle = () => {
    const [word, setWord] = useState<data[]>([
        { alph: '1', type: [AnsType.EMPTY, AnsType.EMPTY, AnsType.EMPTY, AnsType.EMPTY, AnsType.EMPTY] },
        { alph: '1', type: [AnsType.EMPTY, AnsType.EMPTY, AnsType.EMPTY, AnsType.EMPTY, AnsType.EMPTY] },
        { alph: '1', type: [AnsType.EMPTY, AnsType.EMPTY, AnsType.EMPTY, AnsType.EMPTY, AnsType.EMPTY] },
        { alph: '1', type: [AnsType.EMPTY, AnsType.EMPTY, AnsType.EMPTY, AnsType.EMPTY, AnsType.EMPTY] },
        { alph: '1', type: [AnsType.EMPTY, AnsType.EMPTY, AnsType.EMPTY, AnsType.EMPTY, AnsType.EMPTY] },
        { alph: '1', type: [AnsType.EMPTY, AnsType.EMPTY, AnsType.EMPTY, AnsType.EMPTY, AnsType.EMPTY] }
    ]);
    const [userAnswer, setUserAns] = useState<string>('');
    const [answer, setAnswer] = useState<string>('');
    const [counter, setAttempt] = useState(0);
    const [isReconstruction, setReconstruction] = useState(false);
    const today = new Date().toDateString().replace(/ /g, "");

    useEffect(() => {
        async function init() {
            axios.get(`api/mongodb?date=${today}`).then(res => {
                if (res.data.success) {
                    setAnswer(res.data.word.word.toUpperCase());
                    reconstruct().then((result) => {
                        if (result) {
                            setReconstruction(true);
                        }
                    });
                } else {
                    localStorage.clear();
                    setNewWord();
                }
            })
        }
        init();
    }, []);

    useEffect(() => {
        if (isReconstruction) {
            checkAnsType();
        }
    }, [isReconstruction])

    const checkAns = (event: any) => {
        if (event.type === "keyup" && event.key !== 'Enter') {
            return;
        }
        checkAnsType();
        setUserAns('');
        localStorage.setItem(today, JSON.stringify(word));
        localStorage.setItem(today + '_attempts', JSON.stringify(counter));
    }


    const reconstruct = async () => {
        if (localStorage.getItem(today)) {
            let storeData = await JSON.parse(localStorage.getItem(today) || '{}');
            setWord(storeData);
            let storeAttempts = await JSON.parse(localStorage.getItem(today + '_attempts') || '0')
            setAttempt(+storeAttempts);
            toast('Welcome Back!');
            return true;
        } else {
            toast('Best of luck!');
        }
        return false;
    }

    const setNewWord = async () => {
        axios.get('/api/randomWord').then(res => {
            axios.post('api/mongodb', {
                date: new Date().toDateString().replace(/ /g, ""),
                word: res.data.word.toUpperCase()
            }).then(() => {
                setAnswer(res.data.word.toUpperCase());
                toast('Best of luck!');
            })
        })
    }

    const alphabetsOnly = (event: any) => {
        if (event.target.value !== "" && event.target.value.match(/^[A-Za-z]+$/)) {
            word[counter].alph = event.target.value.toUpperCase();
        } else {
            word[counter].alph = "";
        }
        setUserAns(word[counter].alph);
        setWord([...word]);
    }

    const checkAnsType = () => {
        if (word[counter].alph === answer) {
            for (let i = 0; i < answer.length; i++) {
                word[counter].type[i] = AnsType.CORRECT;
            }
            setAttempt(6);
            toast.success('Well Done!');
            return;
        }
        for (let i = 0; i < answer.length; i++) {
            let currentChar = word[counter].alph.charAt(i);
            word[counter].type[i] = currentChar === answer.charAt(i) ? AnsType.CORRECT
                : answer.indexOf(currentChar) > -1 ? AnsType.PARTIAL : AnsType.WRONG;
        }
        setAttempt(counter + 1);
        if (counter >= 5) {
            toast.error(answer);
        }
    }

    return (
        <>
            <Head>
                <title>Wordle- My Version</title>
                <meta name="description" content="My version of Wordle" />
            </Head>
            <div className={styles.centre}>
                <div className={styles.content}>
                    {answer.length === 0 && <LoaderIcon></LoaderIcon>}
                    <ToastContainer position='bottom-center' delay={1500} />
                    {answer.length !== 0 &&
                        <div>
                            <h1>Word of Day!</h1>
                            <ModalScreen title='How to play?'>
                                <p>Guess the correct Word in six tries.</p>
                                <p>Each guess must be a valid five-letter word. Hit the enter button to submit.</p>
                                <p>After each guess, the color of the tiles will change to show how close your guess was to the word.</p>
                                <hr></hr>
                                <p>For Example!</p>
                                <div className='flex'>
                                    <Tile word={'S'} ansType={AnsType.CORRECT}></Tile>
                                    <Tile word={'M'} ansType={AnsType.WRONG}></Tile>
                                    <Tile word={'I'} ansType={AnsType.PARTIAL}></Tile>
                                    <Tile word={'L'} ansType={AnsType.WRONG}></Tile>
                                    <Tile word={'E'} ansType={AnsType.CORRECT}></Tile>
                                </div>
                                <p>The letters S and E are in the word and are in the correct spots.</p>
                                <p>The letter I is in the word but not in the correct spot.</p>
                                <p>The letters M and L are not in the word.</p>
                                <hr></hr>
                                A new Word will be available each day!
                            </ModalScreen>
                            <div>
                                {word.map((item) => {
                                    return <Line word={item.alph} ansType={item.type} key={Math.random() * 100}></Line>
                                })}
                            </div>

                            {counter < 6 &&
                                <>
                                    <p><input type="text" placeholder='Enter your guess!' value={userAnswer.toUpperCase()} onChange={alphabetsOnly} onKeyUp={checkAns}></input></p>
                                    <p></p>
                                    <button onClick={checkAns} disabled={userAnswer.length < 5}>Submit</button>
                                </>
                            }
                        </div>
                    }
                </div>
            </div>
        </>
    )
}

export default Puzzle