// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import words from './five.json'

type Data = {
  name: string[]
}

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {

  let fiveWords = words.filter((word)=> word.length===5);
  res.status(200).json({ name: fiveWords })
}
