import type { NextApiRequest, NextApiResponse } from 'next'
import { connectToDatabase } from "../../mongodb/connection"
const ObjectId = require('mongodb').ObjectId;

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
    // switch the methods
    switch (req.method) {
        case 'GET': {
            return getWOD(req, res);
        }

        case 'POST': {
            return addWod(req, res);
        }

        case 'PUT': {
            //return updatePost(req, res);
        }

        case 'DELETE': {
            //return deletePost(req, res);
        }
    }
}

async function getWOD(req: NextApiRequest, res: NextApiResponse) {
    try {
        let body =req.body;
        // connect to the database
        let { db } = await connectToDatabase();
        // fetch the posts
        let posts = await db
            .collection('wod')
            .findOne({ date: req.query.date})
        // return the posts
        if (posts === null) {
            return res.json({
                success: false,
            })
        } else return res.json({
            word: JSON.parse(JSON.stringify(posts)),
            success: true,
        });
    } catch (error) {
        // return the error
        return res.json({
            message: error,
            success: false,
        });
    }
}

async function addWod(req: NextApiRequest, res: NextApiResponse) {
    try {
        let body =req.body;
        // connect to the database
        let { db } = await connectToDatabase();
        // add the post
        let result = await db.collection('wod').insertOne({
            date: body.date,
            word: body.word
        });
        // return a message
        return res.json({
            message: result,
            success: true,
        });
    } catch (error) {
        // return an error
        return res.json({
            message: error,
            success: false,
        });
    }
}


async function getPosts(req: NextApiRequest, res: NextApiResponse) {
    try {
        // connect to the database
        let { db } = await connectToDatabase();
        // fetch the posts
        let posts = await db
            .collection('wod')
            .find({ date: new Date().toDateString().replace(/ /g, "") })
            .sort({ published: -1 })
            .toArray();
        // return the posts
        return res.json({
            word: JSON.parse(JSON.stringify(posts)),
            success: true,
        });
    } catch (error) {
        // return the error
        return res.json({
            message: error,
            success: false,
        });
    }
}



async function addPost(req: NextApiRequest, res: NextApiResponse) {
    try {
        // connect to the database
        let { db } = await connectToDatabase();
        // add the post
        await db.collection('all-words').insertOne({
            'a': ["abris", "abuse", "abuts", "abuzz", "abyes", "abysm", "abyss", "acari", "acerb", "aceta", "ached", "aches", "achoo", "acids", "acidy", "acing", "acini", "ackee", "acmes", "acmic", "acned", "acnes", "acock", "acold", "acorn", "acred", "acres", "acrid", "acted", "actin", "actor", "acute", "acyls", "adage", "adapt", "addax", "added", "adder", "addle", "adeem", "adept", "adieu", "adios", "adits", "adman", "admen", "admit", "admix", "adobe", "adobo", "adopt", "adore", "adorn", "adown", "adoze", "adult", "adunc", "adust", "adyta", "adzed", "adzes", "aecia", "aedes", "aegis", "aeons", "aerie", "afars", "affix", "afire", "afoot", "afore", "afoul", "afrit", "after", "again", "agama", "agape", "agars", "agate", "agave", "agaze", "agene", "agent", "agers", "agger", "aggie", "aggro", "aghas", "agile", "aging", "agios", "agism", "agist", "agita", "aglee", "aglet", "agley", "aglow", "agmas", "agone", "agons", "agony", "agora", "agree", "agria", "agues", "ahead", "ahing", "ahold", "ahull", "aided", "aider", "aides", "ailed", "aimed", "aimer", "aioli", "aired", "airer", "airns", "airth", "airts", "aisle", "aitch", "aiver", "ajiva", "ajuga", "akees", "akela", "akene", "alack", "alamo", "aland", "alane", "alang", "alans", "alant", "alarm", "alary", "alate", "albas", "album", "alcid", "alder", "aldol", "alecs", "alefs", "aleph", "alert", "alfas", "algae", "algal", "algas", "algid", "algin", "algor", "algum", "alias", "alibi", "alien", "alifs", "align", "alike", "aline", "alist", "alive", "aliya", "alkie", "alkyd", "alkyl", "allay", "allee", "alley", "allod", "allot", "allow", "alloy", "allyl", "almah", "almas", "almeh", "almes", "almud", "almug", "aloes", "aloft", "aloha", "aloin", "alone", "along", "aloof", "aloud", "alpha", "altar", "alter", "altho", "altos", "alula", "alums", "alway", "amahs", "amain", "amass", "amaze", "amber", "ambit", "amble", "ambos", "ambry", "ameba", "ameer", "amend", "amens", "ament", "amias", "amice", "amici", "amide", "amido", "amids", "amies", "amiga", "amigo", "amine", "amino", "amins", "amirs", "amiss", "amity", "ammos", "amnia", "amnic", "amnio", "amoks", "amole", "among", "amort", "amour", "amped", "ample", "amply", "ampul", "amuck", "amuse", "amyls", "ancho", "ancon", "andro", "anear", "anele", "anent", "angas", "angel", "anger", "angle", "anglo", "angry", "angst", "anile", "anils", "anima", "anime", "animi", "anion", "anise", "ankhs", "ankle", "ankus", "anlas", "annal", "annas", "annex", "annoy", "annul", "anoas", "anode", "anole", "anomy", "ansae", "antae", "antas", "anted", "antes", "antic", "antis", "antra", "antre", "antsy", "anvil", "anyon", "aorta", "apace", "apart", "apeak", "apeek", "apers", "apery", "aphid", "aphis", "apian", "aping", "apish", "apnea", "apods", "aport", "appal", "appel", "apple", "apply", "apres", "apron", "apses", "apsis", "apter", "aptly", "aquae", "aquas", "araks", "arame", "arbor", "arced", "arcus", "ardeb", "ardor", "areae", "areal", "areas", "areca", "areic", "arena", "arene", "arepa", "arete", "argal", "argil", "argle", "argol", "argon", "argot", "argue", "argus", "arhat", "arias", "ariel", "arils", "arise", "arles", "armed", "armer", "armet", "armor", "aroid", "aroma", "arose", "arpen", "arras", "array", "arris", "arrow", "arses", "arsis", "arson", "artal", "artel", "artsy", "arums", "arval", "arvos", "aryls", "asana", "ascot", "ascus", "asdic", "ashed", "ashen", "ashes", "aside", "asked", "asker", "askew", "askoi", "askos", "aspen", "asper", "aspic", "aspis", "assai", "assay", "asset", "aster", "astir", "asyla", "ataps", "ataxy", "atilt", "atlas", "atman", "atmas", "atoll", "atoms", "atomy", "atone", "atony", "atopy", "atria", "atrip", "attar", "attic", "audad", "audio", "audit", "auger", "aught", "augur", "aulic", "aunts", "aunty", "aurae", "aural", "aurar", "auras", "aurei", "aures", "auric", "auris", "aurum", "autos", "auxin", "avail", "avant", "avast", "avens", "avers", "avert", "avgas", "avian", "avion", "aviso", "avoid", "avows", "await", "awake", "award", "aware", "awash", "awful", "awing", "awned", "awoke", "awols", "axels", "axial", "axile", "axils", "axing", "axiom", "axion", "axite", "axled", "axles", "axman", "axmen", "axone", "axons", "ayahs", "ayins", "azans", "azide", "azido", "azine", "azlon", "azoic", "azole", "azons", "azote", "azoth", "azuki", "azure"]
        });
        // return a message
        return res.json({
            message: 'Post added successfully',
            success: true,
        });
    } catch (error) {
        // return an error
        return res.json({
            message: error,
            success: false,
        });
    }
}