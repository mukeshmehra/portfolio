import type { NextApiRequest, NextApiResponse } from 'next'

export interface Quote {
    data: Datum;
}

export interface Datum {
    q: string;
    a: string;
    h: string;
}

const api_url = "https://zenquotes.io/api/today";


export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<Quote>
) {

    const response = await fetch(api_url);
    let data = await response.json() ;
    res.status(200).json(data)
}