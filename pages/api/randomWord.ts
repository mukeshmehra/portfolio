// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import words from './five.json'

type Data = {
  word: string
}

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  let fiveWord = words[Math.floor(Math.random()*words.length)];
  res.status(200).json({ word: fiveWord })
}
