import type { NextPage } from 'next'
import Head from 'next/head'
import styles from '../styles/Portfolio.module.css';
import profilePic from '../public/images/dp.jpg'
import Image from "next/image";
import linkedin from '../public/images/linkedin.png'

const Home: NextPage = () => {
  return <>
    <Head>
      <title>Mukesh Mehra</title>
      <meta name="description" content="Portfolio created by Mukesh Mehra" />
    </Head>
    <div className="container">
      <main className={styles.main} id="top">
        <h1 className={styles.title}>Mukesh Mehra</h1>
        <div className={styles.displayPic}>
          <Image
            src={profilePic}
            alt="Picture of the author"
            width={180}
            height={200}
          />
        </div>

        <h3 className={styles.mobEmail}><em>9873468701</em></h3>
        <h3 className={styles.mobEmail}><em>mukesh.mehra85@gmail.com</em></h3>
        <div className={styles.linked}>
          <a href="https://www.linkedin.com/in/mukeshthebest/" target="_blank" rel="noopener noreferrer" >
            Linked In
            <span>
              <Image src={linkedin} alt="Linked In profile"></Image>
            </span>
          </a>
        </div>
        <div>
        <a className={styles.linked}  href="https://drive.google.com/file/d/1LBMkPGh5Ju-QBB0-Cwvyog4Z9o4T9uM7/view?usp=sharing" target="_blank" rel="noreferrer">
         Download Resume <i className="fa fa-download fa-2x" aria-hidden="true"></i>
        </a>
        </div>


        <p className={styles.description}>
          I am India based Developer with experience of more than 12 years of coding in Javascript, Typescript and NodeJs.
        </p>

        <article className={styles.grid} id="skills">

          <div className={styles.card}>
            <h2>Roles & Responsibilities</h2>
            <hr></hr>
            <p>Inter team communication</p>
            <p>Giving Scrum updates</p>
            <p>Doing Code reviews and optimizing the code (Team Mates)</p>
            <p>Giving and taking feedback of team members</p>
          </div>


          <div className={styles.card}>
            <h2>Skills I have</h2>
            <hr></hr>
            <p>Java Script</p>
            <p>Typescript</p>
            <p>Node Js</p>
            <p>React Js</p>
            <p>Next Js</p>
          </div>

          <div className={styles.card}>
            <h2>Tools I use</h2>
            <hr></hr>
            <p>JIRA</p>
            <p>Jenkins, Sonar Qube</p>
            <p>SVN, Maven</p>
            <p>NetBeans, Visual Studio</p>
            <p>Lotus Notes</p>
          </div>
        </article>


        <article className={styles.grid} id="experience">
          <h2>Work Experience</h2>
          <div className={styles.card}>
            <h2>Merkur Gaming India Pvt. Ltd,  Noida</h2>
            <em>Tech Lead, <span className={styles.leftSpace}>April 2015 - PRESENT</span></em>
            <hr></hr>
            <p>Complete project handling from initialization to acceptance phase.</p>
            <p>Handled team of 10+ members</p>
            <p>Took care of code reviews and project quality</p>
            <p>Keeping a tab on all process related activities</p>
          </div>


          <div className={styles.card}>
            <h2>FCS Ltd, Noida</h2>
            <em>Sr. Software Developer, <span className={styles.leftSpace}>Nov 2011 - Mar 2015</span></em>
            <hr></hr>
            <p>Making reusable templates for interactivities and training the team how to use them</p>
            <p>Making assessments and knowledge checks and coding all the interactivities</p>
            <p>Making the course SCORM compatible, LMS.</p>
          </div>

          <div className={styles.card}>
            <h2>PMS Pvt. Ltd</h2>
            <em>Flash Developer, <span className={styles.leftSpace}>Oct 2010 - Nov 2011</span></em>
            <hr></hr>
            <p>Made course framework according to client specifications</p>
            <p>Coding the Simulations and Try Its</p>
          </div>
        </article>
      </main>
      <footer><hr></hr>
      </footer>
    </div>
  </>
}

export default Home
