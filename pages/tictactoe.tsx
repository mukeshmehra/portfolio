import React, { useEffect, useState } from 'react'
import type { NextPage } from 'next'
import LoaderIcon from '../components/loaderIcon'
import Head from 'next/head'
import styles from '../styles/Tictactoe.module.css';
import { TicTacToeLogic } from '../components/tictactoe/tictactoe';
const TicTacToe: NextPage = () => {
    const logic = new TicTacToeLogic();
    enum Players {
        PlayerX = "X",
        PlayerO = "O"
    };
    const [selections, setSelection] = useState<string[]>(new Array(9).fill(""));
    const [currentPlayer, setCurrentPlayer] = useState<Players>(Players.PlayerO);
    const [winningCells, setWinningCells] = useState<number[]>([]);
    const [gameActive, setIsGameActive] = useState<boolean>(true);
    useEffect(() => {

    }, [selections])

    function onCellClick(index: number): void {
        if (gameActive) {
            let newValues = [...selections];
            if (newValues[index] === "") {
                newValues[index] = currentPlayer;
                setSelection(newValues);
                logic.selected = newValues;
                if (logic.handleResultValidation()) {
                    console.log(`Player ${currentPlayer} has won!!`)
                    setIsGameActive(false);
                    setWinningCells(logic.getWinnningCells());
                } else if (!newValues.includes("")) {
                    console.log("Its a draw!!")
                    setIsGameActive(false);
                }
                setCurrentPlayer(currentPlayer === Players.PlayerO ? Players.PlayerX : Players.PlayerO);
            }
        }
    }
    const reset = () => {
        setSelection(new Array(9).fill(""));
        setWinningCells([]);
        setIsGameActive(true)
    }

    return <><Head>
        <title>0-X-0</title>
        <meta name="description" content="A new quote is fetched everyday!" />
    </Head>
        <div className={styles.centre}>
            <h1>Tic Tac Toe</h1>
            <div className={styles.grid}>
                {selections.map((value, index) => {
                    return <div key={index} className={`${styles.cell} ${winningCells.includes(index) ? styles.won : ''}`} onClick={() => onCellClick(index)}>{value}</div>
                })}
            </div>
            <div className={styles.status}>
                <p>Next Turn</p>
                <p>
                    <span className={`${currentPlayer === Players.PlayerO ? styles.active : ''}`}>{Players.PlayerO}</span>
                    <span className={`${currentPlayer !== Players.PlayerO ? styles.active : ''}`}>{Players.PlayerX}</span>
                </p>

            </div>
            {!gameActive && <button onClick={() => reset()}>Reset</button>}
        </div>
    </>
}
export default TicTacToe;
