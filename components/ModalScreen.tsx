import React, { MouseEventHandler, useState } from 'react'
import styles from '../styles/Modal.module.css';

type propTypes = {
  title: string,
  children: any,
}
const ModalScreen = (props: propTypes) => {
  const [show, setShow] = useState<boolean>(false)
  
  return (
    <>
      {!show && <div className={styles.btn} onClick={()=>{setShow(true)}}><p>?</p></div>}
      {show && <div className={styles.modal} onClick={(e:any)=>{
        if(e.target.className==="Modal_close-btn__Dpohw"|| e.target.className==="Modal_modal__yDLSi"){
          setShow(false)
        }}}>
          <div className={styles["modal-content"]}>
            <span className={styles["close-btn"]} onClick={()=>{setShow(false)}}>&times;</span>
            <p>{props.title}</p>
            <hr></hr>
            {props.children}
          </div>
        </div>}
    </>
  )
}
export default ModalScreen