let events = require('events');
export class TicTacToeLogic {
    private selections: string[] = [];
    private winningConditions = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6]
    ];
    private winningCondition:number[]=[];
    public handleResultValidation = ():boolean => {
        let roundWon = false;
        for (let i = 0; i <= 7; i++) {
            const winCondition = this.winningConditions[i];
            let a = this.selections[winCondition[0]];
            let b = this.selections[winCondition[1]];
            let c = this.selections[winCondition[2]];
            if (a === '' || b === '' || c === '') {
                continue;
            }
            if (a === b && b === c) {
                roundWon = true;
                this.winningCondition=winCondition;
                break
            }
        }
       return roundWon;
    }

    
    getWinnningCells():number[]{
        return this.winningCondition;
    }
    set selected(state) {
        this.selections = state;
    }
    get selected(): string[] {
        return this.selections;
    }
}