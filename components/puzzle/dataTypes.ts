export enum AnsType {
    EMPTY = 0,
    FILLED,
    PARTIAL,
    CORRECT,
    WRONG
  }
export type data={
    alph:string,
    type:AnsType[]
}