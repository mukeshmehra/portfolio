import React from 'react'
import PropTypes from 'prop-types'
import styles from '../../styles/Puzzle.module.css';
import { AnsType } from './dataTypes';

type propTypes = {
    word: string,
    ansType: AnsType
}
const Tile = (props: propTypes) => {
    let classes = styles.cell;
    if (props.word === ""|| props.ansType === AnsType.EMPTY) {
        classes += " " + styles.empty;
        return <div className={classes}>{props.word}</div>
    } else {
         if (props.ansType === AnsType.WRONG) {
            classes += " " + styles.wrong;
        } else if (props.ansType === AnsType.PARTIAL) {
            classes += " " + styles.partial;
        } else if (props.ansType === AnsType.CORRECT) {
            classes += " " + styles.correct;
        }
        return (
            <div className={classes}>{props.word}</div>
        )
    }
}

Tile.propTypes = {
    text: PropTypes.string,
    ansType: PropTypes.number
}

export default React.memo(Tile);