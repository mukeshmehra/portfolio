import React from 'react'
import Tile from './Tile';
import styles from '../../styles/Puzzle.module.css';
import { AnsType } from './dataTypes';
type propTypes = {
    word: string,
    ansType: AnsType[]
}
const Line = (props: propTypes) => {
    const list = []
    if (props.word === "1") {
        for (let i = 0; i < 5; i++) {
            list.push(<Tile word={''} ansType={AnsType.EMPTY} key={i} ></Tile>)
        }
    } else {
        for (let i = 0; i < 5; i++) {
            if (i < props.word.length) {
                list.push(<Tile word={props.word.charAt(i)} ansType={props.ansType[i]} key={i} ></Tile>)
            } else {
                list.push(<Tile word={''} ansType={AnsType.EMPTY} key={i} ></Tile>)
            }
        }
    }


    return (
        <div className={styles.grid}>
            {list}
        </div>
    )
}

export default Line