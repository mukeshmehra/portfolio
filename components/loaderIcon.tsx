import React from 'react'
import { ThreeDots as LoadingIcon } from 'react-loading-icons'
const LoaderIcon = () => {
    return (
        <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
            <LoadingIcon fill='#000000' stroke="#000000" />
        </div>
    )
}

export default  LoaderIcon;