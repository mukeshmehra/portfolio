import { Db, MongoClient,MongoClientOptions, ServerApiVersion } from 'mongodb';

const MONGODB_URI = "mongodb+srv://mukesh-random-word:90s7286OHPszsIHo@cluster0.hzmre.mongodb.net/words-app?retryWrites=true&w=majority";

//process.env.MONGODB_URI;
const MONGODB_DB = "words-app";
//process.env.DB_NAME;

// check the MongoDB URI
if (!MONGODB_URI) {
    throw new Error('Define the MONGODB_URI environmental variable');
}

// check the MongoDB DB
if (!MONGODB_DB) {
    throw new Error('Define the MONGODB_DB environmental variable');
}

let cachedClient:MongoClient|null=null;
let cachedDb:Db|null = null;

export async function connectToDatabase() {
    // check the cached.
    if (cachedClient && cachedDb) {
        // load from cache
        return {
            client: cachedClient,
            db: cachedDb,
        };
    }

    // set the connection options
    const opts:MongoClientOptions= {
        serverApi: ServerApiVersion.v1
    };

    // Connect to cluster
    let client = new MongoClient(MONGODB_URI, opts);
    await client.connect();
    let db = client.db(MONGODB_DB);

    // set cache
    cachedClient = client;
    cachedDb = db;

    return {
        client: cachedClient,
        db: cachedDb,
    };
}